# home-acq

Sistema de adquisición y visualización de datos hogareño.

Los criterios de diseño son:

* Programación sobre una __Raspi 4__ 
* Flujo de datos sobre red interna, sin passwords ni certificados
* Programación en Python 3 

## 1. Instalación

### Raspberry Pi
* [instalar](https://www.raspberrypi.com/software/) en una tarjeta _Raspberry Pi OS_ 
  > abrir el puerto SSH
* actualizar el sistema
  ```bash
  me@raspi:~ $ sudo apt update
  me@raspi:~ $ sudo apt dist-upgrade
  ```

### MQTT
* instalar
  ```bash
  me@raspi:~ $ sudo apt install mosquitto
  me@raspi:~ $ sudo apt install mosquitto-clients
  me@raspi:~ $ pip install paho-mqtt --break-system-packages
  ```

* configurar: en `/etc/mosquitto/conf.d/home-acq.conf`
  ```conf
  listener 1883
  allow_anonymous true
  ```
* restart
  ```bash
  me@raspi:~ $ sudo systemctl restart mosquitto.service
  ```
* check
  ```bash
  ## Terminal 1 - Listener
  me@raspi:~ $ mosquitto_sub -v -t "#"

  ## Terminal 2 - Publisher
  me@myOS:~ $ mosquitto_pub -h raspi.local -t pepe -m "Helou"
  ```

### Heartbeat

Tick de un dato por segunto para chequear que el sistema está procesando datos. 

* instalar   
  * copiar archivos `acq.conf`, `heartbeat.py` a `/home/willy/Public`
  * darle a `heartbeat.py` permisos de ejecución
  * copiar `heartbeat.service` a `/etc/systemd/system/heartbeat.service`
  * comandos:
    ```bash
    me@raspi:~ $ sudo systemctl daemon-reload
    me@raspi:~ $ sudo systemctl enable heartbeat.service
    me@raspi:~ $ sudo reboot
    ```
* check
  ```bash
  me@raspi:~ $ mosquitto_sub  -v -t acq/heartbeat
  ```

### InfluxDB

* instalar
  ```bash
  me@raspi:~ $ sudo apt install influxdb influxdb-client
  me@raspi:~ $ sudo systemctl unmask influxdb.service
  me@raspi:~ $ sudo systemctl start influxdb
  me@raspi:~ $ sudo systemctl enable influxdb.service
  ```

* configurar  
  tipear el comando `influx` y ante el prompt configurar
  ```
  > create database acq
  > use acq
  > create user grafana with password 'grafana' with all privileges
  > grant all privileges on acq to grafana
  > show users
  ```
  El output tiene que ser: 
  ```text
  user    admin
  ----    -----
  grafana true
  ```

### Grafana

* instalar
  ```bash
  # esto no está claro que haga falta
  wget -O- https://packages.grafana.com/gpg.key | gpg --dearmor | sudo tee /usr/share/keyrings/grafana-archive-keyring.gpg >/dev/null
  echo "deb [signed-by=/usr/share/keyrings/grafana-archive-keyring.gpg] https://packages.grafana.com/oss/deb stable main" | sudo tee /etc/apt/sources.list.d/grafana.list
  sudo apt update

  # esto sí
  sudo apt install -y grafana
  sudo systemctl unmask grafana-server.service
  sudo systemctl start grafana-server
  sudo systemctl enable grafana-server.service
  ```

* check & config  
  * abrir browser en la red en la URL: `http://raspi.local:3000`   
  * entrar con `admin::admin`
  * en la página principal `Connections -> Add new connection`
    * elegir `Influxdb`
    * en __HTTP__:
      | |  |
      |:--|:---|
      |__URL__| http://localhost:8086 _(<-set !)_|
    
    * en __InfluxDB Details__:
      | |  |
      |:--|:---|
      |__Database__| acq |
      |__User__    | grafana |
      |__Password__ | grafana | 
      |__HTTP Method__ | GET |
      
  <font style="background-color:blue">__&nbsp;&nbsp;Save & Test&nbsp;&nbsp;__</font>

### Dispatch
  ...
&nbsp;      
&nbsp;     
&nbsp;     
&nbsp;     
&nbsp;     
---------------------------      
-------------------
## Getting started



## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

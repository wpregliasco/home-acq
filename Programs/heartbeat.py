#!/usr/bin/python

import datetime as dt
from   time import sleep
import json
import paho.mqtt.publish as publish
from configparser import ConfigParser

#####################################################
conf = ConfigParser()
conf.read('acq.conf')

topic  = conf['Mqtt']['topic'] 
broker = conf['Mqtt']['url']       
portN  = int(conf['Mqtt']['port']) 
#####################################################

while True:
   t     = dt.datetime.now()
   msg   = {'timestamp': t.isoformat(),
            'secs'     : t.second,
           }

   try:
      publish.single(topic, 
                     json.dumps(msg),
                     port = portN, 
                     hostname = broker)	
   except:
      print('Communication failed.')

   sleep(2)

